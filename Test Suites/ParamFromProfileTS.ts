<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ParamFromProfileTS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>32bbde47-1453-4aaf-a595-85e291c79aae</testSuiteGuid>
   <testCaseLink>
      <guid>38a80731-0d32-4f57-b7e9-844f74685d2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VariableFromProfile/ForecastCntVariable</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>64b91af6-1657-4923-8e4d-ab2303711743</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>044a7318-e4ed-4091-bbe5-53268db49407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VariableFromProfile/AirPollutionVariable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c592ff1-43cb-469d-814e-66eec469aa93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VariableFromProfile/MoscowForecastCityVariable</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
