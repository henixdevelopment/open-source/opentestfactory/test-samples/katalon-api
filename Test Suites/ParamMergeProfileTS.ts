<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ParamMergeProfileTS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e860fbab-0e52-4b53-aa62-3c3d6cc928cc</testSuiteGuid>
   <testCaseLink>
      <guid>be6d9f00-49d7-4d2c-8ac6-69422ac306b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parameter/Community</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a94fc40a-a1f6-414b-9813-5414164f575c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parameter/Premium</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>735906f4-5468-4a85-b4d1-98ca5ab3e5fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VariableFromProfile/ForecastCntVariable</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>64b91af6-1657-4923-8e4d-ab2303711743</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2352f9c1-d66d-48e5-9299-cef9800c843d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VariableFromProfile/AirPollutionVariable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22eec738-95fd-4968-be42-6ed420d1b35b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VariableFromProfile/MoscowForecastCityVariable</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
