<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AirPollSuiteKO</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>73df5b7b-d92b-4311-b78a-bcb3bebe6054</testSuiteGuid>
   <testCaseLink>
      <guid>b0ce85b8-2da3-4db5-9fe8-2fa38d14e3f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AirPollutionOK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61e56616-8974-4dbe-8d89-e473fe7cf494</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AirPollIncorrectLocator</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
