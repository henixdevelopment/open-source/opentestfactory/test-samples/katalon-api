Ce projet Katalon est structuré de la manière suivante :

```
ParamColl (TestSuiteCollection)
|
|-NoParamOK (TestSuite)
  |
  |-ForecastOK (TestCase)
|
|-AllParam (TestSuite)
  |
  |-Parameter/Community (TestCase)
  |
  |-Parameter/Premium (TestCase)
  |
  |-Parameter/ExistingParameter (TestCase)
|
NoParamCollKO (TestSuiteCollection)
|
|-ForecastSuiteOK (TestSuite)
  |
  |-ForecastOK (TestCase)
  |
  |-MoscowForecastOK (TestCase)
|
|-AirPollSuiteKO (TestSuite)
  |
  |-AirPollutionOK (TestCase)
  |
  |-AirPollIncorrectLocator (TestCase)
|
NoParamCollOK (TestSuiteCollection)
|
|-AirPollSuiteOK (TestSuite)
  |
  |-AirPollutionOK (TestCase)
|
|-ForecastSuiteOK (TestSuite)
  |
  |-ForecastOK (TestCase)
  |
  |-MoscowForecastOK (TestCase)
|
NoParamKO (TestSuite)
|
|-HistoryKO (TestCase)
|
|-AirPollIncorrectLocator (TestCase)
|
|-History404 (TestCase)
|
ParamFromProfileCol (TestSuiteCollection)
|
|-ParamFromProfileTS (TestSuite)
  |
  |-ForecastCntVariable (TestCase)
  |
  |-AirPollutionVariable (TestCase)
  |
  |-MoscowForecastCityVariable (TestCase)
|
|-NoParamOK (TestSuite)
  |
  |-...
|
ParamMergeProfileCol (TestSuiteCollection)
|
|-ParamFromProfileTS (TestSuite)
  |
  |-...
|
|-ParamFromTM (TestSuite)
  |
  |-Community (TestCase)
  |
  |-Premium (TestCase)
```

Les paramètres attendus pour community sont les suivants :

DS_param : 800

DSNAME : Clear

TC_CUF_icon : 01n

Les paramètres attendus pour premium sont les suivants :

CPG_CUF_humidity : 100

IT_CUF_sealevel : 1031.04

TS_CUF_grndlevel : 1021.39

Attention : 

Une branche (accent) existe contenant un cas de test et des suites de test avec des accents.

Ceux-ci n'ont pas été ajoutés dans la branche principale car Katalon ne gère pas plusieurs cas :

* Lorsque la suite de test contient un accent, la génération du rapport HTML provoque une NPE.
Le rapport XML et l'archive sont bien générés mais le workflow casse sur le get-file report.html.

* Dans le cas de la branche accent, il n'est également plus possible d'exécuter le projet sous UNIX même si le cas
de test exécuté ne contient pas d'accent. Le test casse au début en essayant de construire les chemins.

Plusieurs profiles sont disponibles pour vérifier notamment la fusion d'un profil existant à celui généré automatiquement par l'orchestrateur :

* default : Profil par défaut, contient les paramètres attendus avec une valeur vide (et existingParameter).

* filled_param : Profil contenant les paramètres attendus avec les bonnes valeurs

* local_param : Profil contenant des paramètres supplémentaires non transmissible via Squash TM (notamment utilisé pour vérifier les merges).

* local param space : Profil avec des paramètres supplémentaires pour vérifier le merge avec un fichier contenant des espaces

* empty : Profil vide

* Katalon_params : Profil ayant le même nom que le profil généré par l'orchestrateur
